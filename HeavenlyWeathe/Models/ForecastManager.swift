import Foundation

class ForecastManager {
    static var shared = ForecastManager()
    private init() {}
    
    func getCity(urlString: String,
                 complition: @escaping (_ cityString: String) -> ()) {
        guard let url = URL(string: urlString) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                let data = data else {
                    return
            }
            do {
                let currentCity = try JSONDecoder().decode(City.self, from: data)
                guard let locality = currentCity.name else {
                    return
                }
                complition(locality)
            } catch let error {
                print(error)
            }
        }
        task.resume()
    }
    
    func getForecastForCoordinates(urlString: String,
                                   complition:@escaping (_ forecast: OneCallForecastWeather) -> ()) {
        guard let url = URL(string: urlString) else {
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard error == nil,
                let data = data else {
                    return
            }
            do {
                let forecast = try JSONDecoder().decode(OneCallForecastWeather.self, from: data)
                complition(forecast)
            } catch let error {
                print(error)
            }
        }
        task.resume()
    }
}
