import Foundation

class DateManager {
    static let shared = DateManager()
    private init() {}
    
    func getConvertedDate(sunriseTimeDouble: Double) -> String {
        let date = Date(timeIntervalSince1970: sunriseTimeDouble)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.short
        dateFormatter.dateStyle = DateFormatter.Style.short
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: date)
    }
    
    func getConvertedDateToDay(date: Int) -> String {
        let date = Date.init(timeIntervalSince1970: TimeInterval(date))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date)
    }
}
