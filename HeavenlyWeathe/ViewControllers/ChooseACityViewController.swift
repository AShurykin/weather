import UIKit

protocol ChooseACityViewControllerDelegate: AnyObject {
    func getNewCityForForecastName(city: City)
}

class ChooseACityViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var majorLabel: UILabel!
    @IBOutlet weak var majorTextField: UITextField!
    
    weak var delegate: ChooseACityViewControllerDelegate?
    var cityObjects: [City] = []
    var cityArray: [String] = []
    var additionalCityArray: [City] = []
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print("\(self) deinitialized")
    }
    
    override func loadView() {
        super.loadView()
        majorLabel.text = enterCityLabelText
        view.backgroundColor = .lightGray
        tableView.backgroundColor = .systemTeal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSwipeRecognizer()
        majorTextField.delegate = self
    }
    
    @objc private func swipeDetected(_sender: UISwipeGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    private func addSwipeRecognizer() {
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipeDetected(_sender:)))
        self.view.addGestureRecognizer(swipeRecognizer)
    }
    
}

extension ChooseACityViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return additionalCityArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseACityTableViewCell", for: indexPath) as? ChooseACityTableViewCell else {
            return UITableViewCell()
        }
        cell.setAttributes(text: additionalCityArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        delegate?.getNewCityForForecastName(city: additionalCityArray[indexPath.row])
        navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else {
            return
        }
        textFildEndEditing(text: text)
        self.tableView.reloadData()
    }
    
    func textFildEndEditing(text: String) {
        for city in self.cityObjects {
            guard let cityName = city.name else {
                return
            }
            if cityName.contains(text) {
                additionalCityArray.append(city)
            }
        }
        self.additionalCityArray.sort(by: { $0.name ?? "" < $1.name ?? "" })
    }
}
