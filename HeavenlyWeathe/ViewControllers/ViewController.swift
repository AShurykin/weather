import UIKit
import CoreLocation

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addForecastButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    var manager = CLLocationManager()
    var cityNameArray: [String] = []
    var forecast: OneCallForecastWeather?
    var forecastArray: [OneCallForecastWeather?] = []
    var cityObjects: [City] = []
    
    override func loadView() {
        super.loadView()
        view.backgroundColor = .lightGray
        tableView.backgroundColor = .systemTeal
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFromJson()
        addForecastButton.isHidden = true
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
    
    @IBAction func addForecastButtonPressed(_ sender: UIButton) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "ChooseACityViewController") as? ChooseACityViewController else {
            return
        }
        controller.delegate = self
        controller.cityObjects = cityObjects
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func getFromJson() {
        DispatchQueue.global().async { [weak self] in
            if let path = Bundle.main.path(forResource: "city.list", ofType: "json") {
                do {
                    let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    let json = try JSONDecoder().decode([City].self, from: data)
                    self?.cityObjects = json
                    DispatchQueue.main.async {
                        self?.activityIndicator.isHidden = true
                        self?.addForecastButton.isHidden = false
                    }
                } catch let err {
                    print(err)
                }
            }
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate, ChooseACityViewControllerDelegate, CLLocationManagerDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecastArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CustomTableViewCell", for: indexPath) as? CustomTableViewCell else {
            return UITableViewCell()
        }
        cell.backgroundColor = .lightText
        cell.cityLabel.adjustsFontSizeToFitWidth = true
        cell.temperatureLabel.adjustsFontSizeToFitWidth = true
        guard let temp = forecastArray[indexPath.row]?.current?.temp else {
            return UITableViewCell()
        }
        let correctTempString: String
        if temp > convertToCelsiusDegreeConstant {
            correctTempString = "+\(String(Int(temp - convertToCelsiusDegreeConstant)))°C"
        } else if temp < convertToCelsiusDegreeConstant {
            correctTempString = "-\(String(Int(temp - convertToCelsiusDegreeConstant)))°C"
        } else {
            correctTempString = "\(String(Int(temp - convertToCelsiusDegreeConstant)))°C"
        }
        cell.setAttributes(cityString: cityNameArray[indexPath.row], temperature: correctTempString)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let controller = storyboard?.instantiateViewController(withIdentifier: "ThirdViewController") as? ThirdViewController else {
            return
        }
        guard let pickedForecast = self.forecastArray[indexPath.row] else {
            return
        }
        controller.cityName = cityNameArray[indexPath.row]
        controller.forecast = pickedForecast
        self.navigationController?.pushViewController(controller, animated: true)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func getNewCityForForecastName(city: City) {
        guard let lat = city.coord?.lat else {
            return
        }
        guard let lon = city.coord?.lon else {
            return
        }
        let oneCallUrlString = oneCallUrlOne + String(lat) + oneCallUrlTwo + String(lon) + localString + local + appidString + apiKey
        let getCityUrlString = cityUrlOne + String(lat) + cityUrlTwo + String(lon) + localString + local + appidString + apiKey
        ForecastManager.shared.getCity(urlString: getCityUrlString) { (name) in
            self.cityNameArray.append(name)
        }
        ForecastManager.shared.getForecastForCoordinates(urlString: oneCallUrlString) { (forecast) in
            self.forecastArray.append(forecast)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {
            return
        }
        manager.stopUpdatingLocation()
        let coordinates = location.coordinate
        let cityLatitudeString = String(coordinates.latitude)
        let cityLongitudeString = String(coordinates.longitude)
        let oneCallUrlString = oneCallUrlOne + cityLatitudeString + oneCallUrlTwo + cityLongitudeString + localString + local + appidString + apiKey
        let getCityUrlString = cityUrlOne + cityLatitudeString + cityUrlTwo + cityLongitudeString + localString + local + appidString + apiKey
        ForecastManager.shared.getCity(urlString: getCityUrlString) { (city) in
            self.cityNameArray.append(city)
            ForecastManager.shared.getForecastForCoordinates(urlString: oneCallUrlString) { (currentForecast) in
                DispatchQueue.main.async {
                    self.forecast = currentForecast
                    self.forecastArray.append(self.forecast)
                    self.tableView.reloadData()
                }
            }
        }
    }
}
