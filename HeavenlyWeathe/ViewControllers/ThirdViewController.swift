import UIKit

class ThirdViewController: UIViewController {
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var cityName: String?
    var forecast: OneCallForecastWeather?
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    deinit {
        print("\(self) deinitialized")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addSwipeRecognizer()
        self.setLabelValues()
    }
    
    @objc private func viewControllerPop(_sender: UISwipeGestureRecognizer) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func addSwipeRecognizer() {
        let swipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(viewControllerPop(_sender:)))
        self.view.addGestureRecognizer(swipeRecognizer)
    }
    
    private func setLabelValues() {
        cityLabel.adjustsFontSizeToFitWidth = true
        guard let forecast = forecast else {
            return
        }
        cityLabel.text = cityName
        if getGuardDouble(value: forecast.current?.temp) - convertToCelsiusDegreeConstant > 0 {
            temperatureLabel.text = "+\(String(countToCelsius(temp: forecast.current?.temp)))°C"
        } else if getGuardDouble(value: forecast.current?.temp) - convertToCelsiusDegreeConstant < 0 {
            temperatureLabel.text = "-\(String(countToCelsius(temp: forecast.current?.temp)))°C"
        } else {
            temperatureLabel.text = "\(String(countToCelsius(temp: forecast.current?.temp)))°C"
        }
//        pressureLabel.text = "Pressure"
//        pressureValueLabel.text = "\(String(getGuardDouble(value: forecast.current?.pressure))) mbar"
//        guard let sunriseTimeInt = forecast.current?.sunrise else {
//            return
//        }
//        guard let sunsetTimeInt = forecast.current?.sunset else {
//            return
//        }
//        let sunriseTimeDouble = Double(sunriseTimeInt)
//        let sunsetTimeDouble = Double(sunsetTimeInt)
//        let sunrise = DateManager.shared.getConvertedDate(sunriseTimeDouble: sunriseTimeDouble)
//        let sunset = DateManager.shared.getConvertedDate(sunriseTimeDouble: sunsetTimeDouble)
//        sunriseLabel.text = "Sunrise"
//        sunsetLabel.text = "Sunset"
//        sunriseValueLabel.text = sunrise
//        sunsetValueLabel.text = sunset
    }
}

extension ThirdViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let forecastDailyCount = forecast?.daily.count else {
            return 0
        }
        return forecastDailyCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ThirdViewControllerTableViewCell", for: indexPath) as? ThirdViewControllerTableViewCell else {
            return UITableViewCell()
        }
        guard let dateInt = forecast?.daily[indexPath.row]?.dt else {
            return UITableViewCell()
        }
        let dateString = DateManager.shared.getConvertedDateToDay(date: dateInt)
        guard let dayTemperature = forecast?.daily[indexPath.row]?.temp?.day else {
            return UITableViewCell()
        }
        guard let nightTemperature = forecast?.daily[indexPath.row]?.temp?.night else {
            return UITableViewCell()
        }
        guard let icon = forecast?.daily[indexPath.row]?.weather[0]?.icon else {
            return UITableViewCell()
        }
        cell.setCellAttributes(dateString: dateString, dayTemperature: dayTemperature, nightTemperature: nightTemperature, iconString: icon)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let days = forecast?.daily.count else {
            return 0
        }
        return tableView.frame.height / CGFloat(days)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard forecast?.hourly.count != nil else {
            return 0
        }
        return oneDay
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ThirdViewControllerCollectionViewCell", for: indexPath) as? ThirdViewControllerCollectionViewCell else {
            return UICollectionViewCell()
        }
        guard let name = forecast?.hourly[indexPath.row]?.weather[0]?.icon else {
                return UICollectionViewCell()
        }
        guard let date = forecast?.hourly[indexPath.row]?.dt else {
            return UICollectionViewCell()
        }
        guard let temp = forecast?.hourly[indexPath.row]?.temp else {
            return UICollectionViewCell()
        }
        cell.setImageAttributes(imageName: name)
        cell.setDateLabelAttributes(text: date)
        cell.setTemperatureLabelAttributes(temp: temp)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.height - 10, height: collectionView.bounds.height - 10)
    }
}
