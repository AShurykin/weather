import Foundation

class Coord: Decodable {
    let lon: Double?
    let lat: Double?
}
