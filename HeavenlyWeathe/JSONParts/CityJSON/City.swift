import Foundation

class City: Decodable {
    
    internal init(name: String?, country: String?, coord: Coord?) {
        self.name = name
        self.country = country
        self.coord = coord
    }
    
    let name: String?
    let country: String?
    let coord: Coord?
}
