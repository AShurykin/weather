import Foundation

class OneCallForecastWeather: Decodable {
    let current: Current?
    let hourly: [Hourly?]
    let daily: [Daily?]
}
