import Foundation

class Hourly: Decodable {
    let dt: Int?
    let temp: Double?
    let pressure: Int?
    let weather: [Weather?]
}
