import Foundation

class Daily: Decodable {
    let dt: Int?
    let sunrise: Int?
    let sunset: Int?
    let temp: Temp?
    let pressure: Int?
    let weather: [Weather?]
}
