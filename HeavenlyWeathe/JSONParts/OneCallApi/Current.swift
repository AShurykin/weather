import Foundation

class Current: Decodable {
    let dt: Int?
    let sunrise: Int?
    let sunset: Int?
    let temp: Double?
    let pressure: Double?
    let weather: [Weather?]
}
