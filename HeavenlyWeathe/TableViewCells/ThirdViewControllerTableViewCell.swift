import UIKit

class ThirdViewControllerTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var dayTemperatureLabel: UILabel!
    @IBOutlet weak var nightTemperatureLabel: UILabel!
    @IBOutlet weak var forecastImageView: UIImageView!
    
    func setCellAttributes(dateString: String,
                           dayTemperature: Double,
                           nightTemperature: Double,
                           iconString: String) {
        dateLabel.text = dateString
        var dayTemperatureString: String
        if dayTemperature - convertToCelsiusDegreeConstant > 0 {
            dayTemperatureString = String("+\(String(round(10*(dayTemperature - convertToCelsiusDegreeConstant))/10))°C")
        } else if dayTemperature - convertToCelsiusDegreeConstant < 0 {
            dayTemperatureString = String("-\(String(round(10*(dayTemperature - convertToCelsiusDegreeConstant))/10))°C")
        } else {
            dayTemperatureString = String("\(String(round(10*(dayTemperature - convertToCelsiusDegreeConstant))/10))°C")
        }
        dayTemperatureLabel.text = dayTemperatureString
        var nightTemperatureString: String
        if nightTemperature - convertToCelsiusDegreeConstant > 0 {
            nightTemperatureString = String("+\(String(round(10*(nightTemperature - convertToCelsiusDegreeConstant))/10))°C")
        } else if nightTemperature - convertToCelsiusDegreeConstant < 0 {
            nightTemperatureString = String("-\(String(round(10*(nightTemperature - convertToCelsiusDegreeConstant))/10))°C")
        } else {
            nightTemperatureString = String("\(String(round(10*(nightTemperature - convertToCelsiusDegreeConstant))/10))°C")
        }
        nightTemperatureLabel.text = nightTemperatureString
        forecastImageView.image = UIImage(named: iconString)
    }
}
