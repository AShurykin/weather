import UIKit

class CustomTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    func setAttributes(cityString: String?, temperature: String?) {
        cityLabel.text = cityString
        temperatureLabel.text = temperature
    }
}
