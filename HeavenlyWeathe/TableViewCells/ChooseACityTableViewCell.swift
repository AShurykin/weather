import UIKit

class ChooseACityTableViewCell: UITableViewCell {
    @IBOutlet weak var cityNameLabel: UILabel!
    
    func setAttributes(text: City) {
        cityNameLabel.text = text.name
    }
}
