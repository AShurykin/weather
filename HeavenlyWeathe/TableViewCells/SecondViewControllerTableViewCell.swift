import UIKit

class SecondViewControllerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    func setAttributes(text: String) {
        cityNameLabel.text = text
    }
}
