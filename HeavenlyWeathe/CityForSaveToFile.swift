//import Foundation
//
//class CityForSaveToFile: Codable {
//    let cityName: String
//    
//    internal init(cityName: String) {
//        self.cityName = cityName
//    }
//    
//    public enum CodingKeys: String, CodingKey {
//        case cityName
//    }
//    
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//        
//        try container.encode(self.cityName, forKey: .cityName)
//    }
//    
//    required public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//        
//        self.cityName = try (container.decodeIfPresent(String.self, forKey: .cityName) ?? "cityName")
//    }
//}
