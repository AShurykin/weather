import UIKit

extension UIViewController {
    func countToCelsius(temp: Double?) -> Double {
        guard let temp = temp else {
            return 0.0
        }
        return Double(round(10*(temp - convertToCelsiusDegreeConstant))/10)
    }
    
    func getGuardDouble(value: Double?) -> Double {
        guard let value = value else {
            return 0.0
        }
        return value
    }
    
    func dateFormate(dateString: String) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm dd.MM.yyyy"
    }
}

extension String {
    
    func locatized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}

