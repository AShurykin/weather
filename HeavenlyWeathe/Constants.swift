import Foundation

let UrlStringOne = "https://api.openweathermap.org/data/2.5/forecast?q="
let appidString = "&appid="
let apiKey = "a2be29762be821f60d281c6fefc4fe45"

let cityUrlOne = "https://api.openweathermap.org/data/2.5/weather?lat="
let cityUrlTwo = "&lon="
let localString = "&lang="

let oneCallUrlOne = "https://api.openweathermap.org/data/2.5/onecall?lat="
let oneCallUrlTwo = "&lon="

let convertToCelsiusDegreeConstant: Double = 273.15

let oneDay = 24

enum Keys: String {
    case fileName = "new"
    case newFileName = "newName"
}
