import UIKit

class ThirdViewControllerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    
    func setImageAttributes(imageName: String) {
        imageView.image = UIImage(named: imageName)
    }
    
    func setDateLabelAttributes(text: Int) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM HH:mm"
        let date = Date.init(timeIntervalSince1970: TimeInterval(text))
        dateLabel.text = dateFormatter.string(from: date)
    }
    
    func setTemperatureLabelAttributes(temp: Double) {
        if temp > convertToCelsiusDegreeConstant {
            temperatureLabel.text = "+\(String(round(10*(temp - convertToCelsiusDegreeConstant))/10))°C"
        } else if temp < convertToCelsiusDegreeConstant {
            temperatureLabel.text = "-\(String(round(10*(temp - convertToCelsiusDegreeConstant))/10))°C"
        } else {
            temperatureLabel.text = "\(String(round(10*(temp - convertToCelsiusDegreeConstant))/10))°C"
        }
    }
}
