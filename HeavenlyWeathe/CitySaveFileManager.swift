//import Foundation
//
//class CitySaveFileManager {
//    static let shared = CitySaveFileManager()
//    private init() {}
//    
//    func stringSaveToFile(cityString: CityForSaveToFile) {
//        let fileManager = FileManager.default
//        if let fileUrlName = UserDefaults.standard.object(forKey: Keys.fileName.rawValue) as? String {
//            do {
//                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                let fileUrl = documentDirectory.appendingPathComponent(fileUrlName)
//                let fileUrlString = try String(contentsOf: fileUrl)
//                guard let cityArray = NSKeyedUnarchiver.unarchiveObject(withFile: fileUrlString) as? [CityForSaveToFile] else {
//                    return
//                }
//                var guardCityArray = cityArray
//                guardCityArray.append(cityString)
//                NSKeyedArchiver.archiveRootObject(guardCityArray, toFile: fileUrlName)
//            } catch let error {
//                print(error)
//            }
//        } else {
//            do {
//                let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
//                let fileUrlName = UUID().uuidString
//                UserDefaults.standard.set(fileUrlName, forKey: Keys.fileName.rawValue)
//                let fileUrl = documentDirectory.appendingPathComponent(fileUrlName)
//                let fileUrlString = try String(contentsOf: fileUrl)
//                let array = [cityString]
//                NSKeyedArchiver.archiveRootObject(array, toFile: fileUrlString)
//            } catch let error {
//                print(error)
//            }
//        }
//    }
//    
//    func openFile(complition: @escaping (_ cityName: [CityForSaveToFile]) -> ()) {
//        guard let fileName = UserDefaults.standard.object(forKey: Keys.fileName.rawValue) as? String else {
//            return
//        }
//        let fileManager = FileManager.default
//        do {
//            let documentDirectory = try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor:nil, create:false)
//            let fileURL = documentDirectory.appendingPathComponent(fileName)
//            let fileUrlString = try String(contentsOf: fileURL)
//            guard let cityArray = NSKeyedUnarchiver.unarchiveObject(withFile: fileUrlString) as? [CityForSaveToFile] else {
//                return
//            }
//            complition(cityArray)
//        } catch {
//            print("Error loading image : \(error)")
//        }
//    }
//}
